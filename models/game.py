from models.deck import Deck
from models.player import Player

class Game:
    def __init__(self):
        self.players = []
        self.deck = Deck()
        self.current_player = 0

    def deal(self):
        # gives 5 cards to each player
        pass

    def take_turn(self):
        # greet the current player

        # print their cards and sets

        # ask which player they'd like to steal from

        # ask what card number they'd like to steal

        # if the player had none of the requested cards, "GO FISH!", draw a card, turn is over.

        # if the player DID have a/some card(s), add them to the current player's hand,
        # allow current player to take another turn

        # increment the current player number, if it's >= number of players, reset to 0.
        pass

    def create_players(self):
        # ask how many players there will be
        # for each player, ask for a name.
        # create player objects, add them to the self.players list.
        pass