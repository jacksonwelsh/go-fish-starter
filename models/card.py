class Card:
    def __init__(self, value, suit):
        self.value = value
        self.suit = suit

    def __str__(self):
        # solve the problem where 13 is king, etc. using a dictionary.
        names = {
            11: 'Jack',
            12: 'Queen',
            13: 'King',
            14: 'Ace'
        }
        name = names.get(self.value, self.value)
        return f'{name} of {self.suit}'
