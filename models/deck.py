from random import choice
class Deck:
    def __init__(self):
        self.cards = []

    def generate(self):
        # loop through all possible card numbers
        # in that loop, loop through all the suits
        # append each card to self.cards
        pass

    def shuffle(self):
        # make a new list that will hold shuffled cards
        # for in range(52)
        # pick a random card from self.cards
        # remove it from self.cards
        # append it to that new list
        # set self.cards = new list
        pass
    
    def draw(self):
        # removes and returns the top card (index 0) of the deck.
        # bonus points for checking if the deck is empty and returning None if so.
        pass
