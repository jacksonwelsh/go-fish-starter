from models.game import Game
# https://www.thesprucecrafts.com/go-fish-card-game-rules-411135
game = Game()
game.create_players()
game.deal()
while True:
  game.take_turn()